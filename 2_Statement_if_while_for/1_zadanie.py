# (if/elif/else)

# По номеру месяца вывести кол-во дней в нем (без указания названия месяца, в феврале 28 дней)
# Результат проверки вывести на консоль
# Если номер месяца некорректен - сообщить об этом

# Номер месяца получать от пользователя следующим образом
# user_input = input("Введите, пожалуйста, номер месяца: ")
# month = int(user_input)
# print('Вы ввели', month)

user_input = input("Введите, пожалуйста, номер месяца: ")
month = int(user_input)

if month == 1 or 3 or 5 or 7 or 8 or 10 or 12:
    print(31)
elif month == 4  or 6 or 9 or 11:
    print(30)
else:
    print(29)

print('Вы ввели', month)